import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    hidden: true,
    children: [{
      path: 'dashboard',
      id:'1',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '主页' }
    }]
  },
  {
    path: '/login',
    id:'2',
    component: () => import('@/views/login/index'),
    hidden: true,
  },{
    path: '/forgetpwd',
    id:'3',
    hidden: true,
    component: () => import('@/views/login/forgetpwd'),
    meta: { title: '忘记密码'}
  },
  { path: '/404', id:'4', component: () => import('@/views/404'), hidden: true },
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

//异步挂载的路由
//动态需要根据权限加载的路由表
//avatar 页面需要的权限  0管理员    2安然打卡专员  3同乐管理员   4安然网管理员
export const asyncRouterMap = [
  {
    path: '/tongle',
    component: Layout,
    meta: { title: '同乐后台', icon: 'example', avatar:['0','3'] },
    children: [
      {
        path: 'index',
        name: 'tongle',
        component: () => import('@/views/wxdata/index'),
        meta: { title: '基本信息', icon: 'form' }
      },
      {
        path: 'model',
        name: 'model',
        component: () => import('@/views/wxdata/model'),
        meta: { title: '模块信息', icon: 'tree' }
      },
      {
        path: 'about',
        name: 'about',
        component: () => import('@/views/wxdata/about'),
        meta: { title: '关于我们', icon: 'user' }
      }
    ]
  },
  {
    path: '/wxqiandao',
    component: Layout,
    name: 'wxqiandao',
    meta: { title: '签到管理', icon: 'table', avatar:['0','2'] },
    children: [
        {
          path: 'signMap',
          name: 'signMap',
          component: () => import('@/views/wxqiandao/signMap'),
          meta: { title: '签到地图', icon: 'map' }
        },
        {
          path: 'userlist',
          name: 'userlist',
          component: () => import('@/views/wxqiandao/userlist'),
          meta: { title: '用户管理', icon: 'user' }
        },{
          path: 'usermsg',
          name: 'usermsg',
          component: () => import('@/views/wxqiandao/usermsg'),
          meta: { title: '用户留言', icon: 'msg' }
        }
      ]
  },
  {
    path: '/anranweb',
    component: Layout,
    name: 'anranweb',
    meta: { title: '安然网', icon: 'tree',avatar:['0', '4'] },
    children: [
      {
        path: 'addnew',
        name: 'addnew',
        component: () => import('@/views/anranweb/addNew'),
        props: (route) => ({ id: route.query.id }),
        meta: { title: '创建文章', icon: 'form'  }
      },{
        path: 'news',
        name: 'news',
        component: () => import('@/views/anranweb/news'),
        meta: { title: '文章管理', icon: 'nested' }
      },{
        path: 'indexmsg',
        name: 'indexmsg',
        component: () => import('@/views/anranweb/indexmsg'),
        meta: { title: '首页留言', icon: 'msg' }
      }
    ]
  },
  {
    path: '/sysset',
    component: Layout,
    name: 'sysset',
    meta: { title: '系统管理', icon: 'sys' },
    children: [
      {
        path: 'user',
        name: 'user',
        component: () => import('@/views/sysSet/user'),
        meta: { title: '用户管理', icon: 'user', avatar:['0'] }
      },
      {
        path: '/eaditPwd',
        name: 'eaditPwd',
        component: () => import('@/views/sysSet/eaditPwd'),
        meta: { title: '修改密码', icon: 'password'}
      }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
];
