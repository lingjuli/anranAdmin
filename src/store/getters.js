const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  permission_routers: state => state.user.routers,
  addRouters: state => state.user.addRouters,
  userInfo: state=> state.user.userInfo,
}
export default getters
