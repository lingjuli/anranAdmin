import request from '@/utils/request'
const qs = require('qs')

let defurl = '/anfile/php/vrv_user.php';

export function login(username, password) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'login',
      username: username,
      password: password
    })
  })
}

export function getInfo(token) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'getInfo',
      token: token
    })
  })
}

export function logout() {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'logout'
    })
  })
}
