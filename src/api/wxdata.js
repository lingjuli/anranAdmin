import request from '@/utils/request'
const qs = require('qs')

let defurl = '/anfile/php/vrv_user.php';

// /*获取基本信息 */
export function getinfodata() {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'getinfodata'
    })
  })
}
// /*修改基本信息 */
export function setinfodata(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'setinfodata',
      data: d
    })
  })
}
// /*查询模块信息 */
export function selecttxt(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'selecttxt',
      data: d
    })
  })
}
// /*修改模块信息 */
export function selecttxtupdata(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'selecttxtupdata',
      data: d
    })
  })
}
// /*查询关于我们图片 */
export function getaboutimg() {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'uspic'
    })
  })
}
// /*关于我们上传图片 */
export function aboutimgupdata(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'aboutimgupdata',
      data: d
    })
  })
}
// /*删除服务器图片 */
export function delectimg(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'delectimg',
      data: d
    })
  })
}

