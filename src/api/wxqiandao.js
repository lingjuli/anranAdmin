import request from '@/utils/request'
const qs = require('qs')
let defurl = '/anfile/php/vrv_user.php';
// /*获取用户信息 */
export function getuserinfo(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'vueselectdata',
      data:d
    })
  })
}
// 查询打卡记录
export function selectuserdaka(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'vueselectaddres',
      data:d
    })
  })
}
// 获取用户留言
export function getusermsg() {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'vueselectmsg'
    })
  })
}
// 删除留言
export function delusermsg(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'vuedeletemsg',
      data:d
    })
  })
}
// 查询首页记录留言
export function getindexusermsg(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'getindexmsg',
      data:d
    })
  })
}
// 删除首页留言
export function delindexmsg(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'delectindexmsg',
      data:d
    })
  })
}

// 查询所有打卡信息
export function VueSelectAllSign(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'vueSelectAllSign',
      data:d
    })
  })
}
