import request from '@/utils/request'
const qs = require('qs');

let defurl = '/anfile/php/username.php';
let anran = '/anfile/php/an_ranweb.php';

export function login(username, password) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'login',
      username: username,
      password: password
    })
  })
}

/* 验证邮箱 */
export function forgetpwdemailselect(email) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'forgetpwdemailselect',
      email: email,
    })
  })
}

/* 发送验证码 */
export function sendemail(email,emailcode) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'sendemail',
      email: email,
      emailcode:emailcode,
    })
  })
}

/* 给邮箱发送账号密码 */
export function forgetpwdemail(email) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'forgetpwdemail',
      email: email,
    })
  })
}

/* 查询用户列表 */
export function selectuser(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'selectuser',
      data:d,
    })
  })
}

/* 新增用户 */
export function addUserReg(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'reg',
      data:d
    })
  })
}

/* 修改用户信息 */
export function updatauser(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'updatauser',
      data:d
    })
  })
}

/* 查询用户名是否重复 */
export function regname(account) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'regname',
      account: account
    })
  })
}

/* 查询邮箱是否被占用 */
export function regemail(email) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'regemail',
      email: email
    })
  })
}

/* 删除用户 */
export function deleteuser(id) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'deleteuser',
      id: id
    })
  })
}

/* 查询用户 */
export function seachUserName(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'seachUserName',
      data: d
    })
  })
}

/* 修改密码 */
export function forgetpwdnew(d) {
  return request({
    url: defurl,
    method: 'post',
    data: qs.stringify({
      q: 'forgetpwdnew',
      data: d
    })
  })
}

/* 角色 */
export function userAvatar(val) {
  if(val === 11){
    return 'APP用户'
  }else{
    let nd = ['超级管理员','同乐管理员','打卡专员','同乐管理员'];
    return nd[val]
  }
}

/* 新增文章 */
export function addArticle(d) {
  return request({
    url: anran,
    method: 'post',
    data: qs.stringify({
      q: 'addArticle',
      data: d
    })
  })
}

/* 验证标题是否重复 */
export function checkarticletil(d) {
  return request({
    url: anran,
    method: 'post',
    data: qs.stringify({
      q: 'checkarticletil',
      data: {
        title:d
      }
    })
  })
}

/* 获取文章列表 */
export function articleList(d) {
  return request({
    url: anran,
    method: 'post',
    data: qs.stringify({
      q: 'articleList',
      data: d
    })
  })
}

/* 文章详情 */
export function articleinfo(id) {
  return request({
    url: anran,
    method: 'post',
    data: qs.stringify({
      q: 'articleinfo',
      data: {
        id_number: id
      }
    })
  })
}

/* 删除文章 */
export function articleDelet(id) {
  return request({
    url: anran,
    method: 'post',
    data: qs.stringify({
      q: 'articleDelet',
      data: {
        id_number: id
      }
    })
  })
}

/* 修改文章状态 */
export function EditArticle(d) {
  return request({
    url: anran,
    method: 'post',
    data: qs.stringify({
      q: 'EditArticle',
      data: d
    })
  })
}

