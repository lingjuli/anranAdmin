import axios from 'axios'
import { Message, MessageBox } from 'element-ui'
import store from '../store'
import { getToken } from '@/utils/auth'

// 创建axios实例
const service = axios.create({
  retry: 5, //最大请求次数
  timeout: 3000 //请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
  if (store.getters.token) {
    config.headers['X-Token'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
  response => {
  /**
  * code为非20000是抛错 可结合自己业务进行修改
  */
    const res = response.data
    if (res.code !== 200) {
      Message({
        message: res.msg,
        type: 'error',
        duration: 5 * 1000
      })

      // 50008:非法的token; 50012:其他客户端登录了;  50014:Token 过期了;
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        MessageBox.confirm('你已被登出，可以取消继续留在该页面，或者重新登录', '确定登出', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('FedLogOut').then(() => {
            location.reload()// 为了重新实例化vue-router对象 避免bug
          })
        })
      }
      return Promise.reject('error')
    } else {
      return response.data
    }
  },
  error => {

    //如果连接超时会再次请求，直到超过最多请求次数
    let config = error.config;
    // If config does not exist or the retry option is not set, reject
    if(!config || !config.retry) return Promise.reject(error);

    // 设置请求次数
    config.__retryCount = config.__retryCount || 0;

    // 是否超过最大请求次数
    if(config.__retryCount >= config.retry) {
      //报错
      console.log('错误：' + error)// for debug
      Message.error('网络故障');
      return Promise.reject(error);
    }

    // 请求次数自增
    config.__retryCount += 1;
    console.log('再次请求：'+ config.__retryCount);

    // 设置定时器 超过请求时间后重新请求
    let backoff = new Promise(function(resolve) {
      setTimeout(function() {
        resolve();
      }, config.timeout || 1);
    });

    // Return the promise in which recalls axios to retry the request
    return backoff.then(function() {
      return service.request(config);
    });
  }
)

export default service
