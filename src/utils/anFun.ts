/* 深度复制 */
export function anCopy(obj:any){
  let newobj:any = {};
  for ( let attr in obj) {
    newobj[attr] = obj[attr];
  }
  return newobj;
}

/* 排序 */
export function format(index:number, Page:number):number{
  index = index + 1;
  let indexSy:number;
  if (Page > 1) {
    let sy = (Page - 1) * 10;
    indexSy = sy + index;
  } else {
    indexSy = index;
  }
  return indexSy;
}
