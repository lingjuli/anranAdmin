/**
 * Created by jiachenpan on 16/11/18.
 */

export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (('' + time).length === 10) time = parseInt(time) * 1000
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    if (key === 'a') return ['一', '二', '三', '四', '五', '六', '日'][value - 1]
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}

export function formatTime(time, option) {
  time = +time * 1000
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) { // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return d.getMonth() + 1 + '月' + d.getDate() + '日' + d.getHours() + '时' + d.getMinutes() + '分'
  }
}

/* 生成随机数  n参数是位数 字符串 */
export function anRandom(n) {
  let rand = "";
  for(let i = 0; i < n; i++){
    let r = Math.floor(Math.random() * 10);
    rand += r;
  }
  return rand
}

/* 生成时间戳 */
export function anTimeGit() {
  return new Date().getTime()
}
/* 解析时间戳 */
export function angetLocalTime(nS) {
  return  new Date(parseInt(nS) * 1000).toLocaleString().replace(/:\d{1,2}$/,' ');
}


/* 排序 */
export function format(index, Page){
  index = index + 1;
  let indexSy;
  if (Page > 1) {
    let sy = (Page - 1) * 10;
    indexSy = sy + index;
  } else {
    indexSy = index;
  }
  return indexSy;
}


/*获取当前时间并格式化时间*/
// DataTimeFormat("yyyy-MM-dd HH:mm:ss")
// DataTimeFormat("yyyy-MM-dd")
//第二个参数为时间参数，传入时间格式化返回   data类型字符串‘2019/2/12’
export function DataTimeFormat (fmt,data) {
  let netime = new Date();
  if(data){
    try {
      netime = new Date(Date.parse(data));
    }catch (err){}
  }
  let o = {
    "M+": netime.getMonth() + 1, //月份
    "d+": netime.getDate(), //日
    "H+": netime.getHours(), //小时
    "m+": netime.getMinutes(), //分
    "s+": netime.getSeconds(), //秒
    "q+": Math.floor((netime.getMonth() + 3) / 3), //季度
    "S": netime.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (netime.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}
